package ql

import (
	"database/sql"
	"fmt"
	"reflect"
	"regexp"
	"strings"

	"gitlab.com/magtil/rpgio/util/logger"
	_ "modernc.org/ql/driver"
)

/**
*暂时只用 INT 和 STRING .......
**/
type Conn struct {
	Db       *sql.DB
	DbFolder string
	DbFile   string
}

var (
	Logger     = logger.Logger{}
	DbFolder   = "./ql/"
	DateFormat = "2006/01/02/15:04:05"
)

//打开数据库
func (c *Conn) Open() {
	db, err := sql.Open("ql", c.DbFile)
	if err != nil {
		Logger.Error(err)
	} else {
		c.Db = db
	}
}

//关闭数据库
func (c *Conn) Close() {
	defer c.Db.Close()
}

//数据查询
func (c *Conn) Query(sql string) []map[string]string {
	var rr = make([]map[string]string, 0)
	reg := regexp.MustCompile(`[']`)
	sql = reg.ReplaceAllString(sql, "`")

	rows, err := c.Db.Query(sql)

	if err != nil {
		Logger.Error(err)
		Logger.Debug(sql)
	} else {
		columns, _ := rows.Columns()
		scanArgs := make([]interface{}, len(columns))
		values := make([]interface{}, len(columns))
		for i := range values {
			scanArgs[i] = &values[i]
		}
		for rows.Next() {
			err = rows.Scan(scanArgs...)
			rp := make(map[string]string)
			for i, col := range values {
				if col != nil {
					if fmt.Sprint(reflect.TypeOf(col)) == "int64" {
						rp[columns[i]] = fmt.Sprint(col)
					} else {
						val := col.([]byte)
						rp[columns[i]] = string(val[:])
					}
				}
			}
			rr = append(rr, rp)
		}
	}
	return rr
}

//数库执行
func (c *Conn) Exec(sql string) int64 {
	tx, err := c.Db.Begin()
	if err != nil {
		Logger.Error(err)
		return 0
	}
	reg := regexp.MustCompile(`[']`)
	sql = reg.ReplaceAllString(sql, "`")
	res, err := tx.Exec(sql)
	if err != nil {
		Logger.Error(err)
		return 0
	} else {
		if err = tx.Commit(); err != nil {
			Logger.Error(err)
			return 0
		}
		reid, _ := res.LastInsertId()
		return reid
	}

}

//获得数据
func (c *Conn) Take(t string, q map[string]string) []map[string]interface{} {
	//查询参数：1 字段=field、2 表名=table、3 WHERE=where、4 LIMIT=limit、5 ORDER=order
	var rr = make([]map[string]interface{}, 0)
	var sql string
	var field string
	if k, ok := q["field"]; ok {
		field = k
	} else {
		field = "*"
	}
	var where string
	if k, ok := q["where"]; ok {
		where = " WHERE " + k
	}
	var limit string
	if k, ok := q["limit"]; ok {
		limit = " LIMIT " + k
	}
	var order string
	if k, ok := q["order"]; ok {
		order = " ORDER BY " + k
	}
	sql = "SELECT " + field + " FROM " + t + where + order + limit
	reg := regexp.MustCompile(`[']`)
	sql = reg.ReplaceAllString(sql, "`")

	//fmt.Printf("SQL Code: %d\nLink:125\n", sql)
	rows, err := c.Db.Query(sql)
	if err != nil {
		Logger.Error(err)
		Logger.Debug(sql)
	} else {
		columns, _ := rows.Columns()
		scanArgs := make([]interface{}, len(columns))
		values := make([]interface{}, len(columns))
		for i := range values {
			scanArgs[i] = &values[i]
		}
		for rows.Next() {
			err = rows.Scan(scanArgs...)
			rp := make(map[string]interface{})
			for i, col := range values {
				if col != nil {
					if fmt.Sprint(reflect.TypeOf(col)) == "int64" {
						rp[columns[i]] = col.(int64)
					} else {
						val := col.([]byte)
						rp[columns[i]] = string(val[:])
					}
				}
			}
			rr = append(rr, rp)
		}
	}
	return rr
}

//保存数据
func (c *Conn) Save(t string, kv map[string]interface{}, u ...string) int64 {
	//保存参数：t=表,kv值,u更新的where
	var returndata int64 = 0
	if len(u) > 0 {
		//Update
		var sql string
		var sqlset = " SET "
		sql = "UPDATE " + t

		for k, v := range kv {
			sqlset = sqlset + k + "=" + v.(string) + ","
		}
		sqlset = strings.TrimRight(sqlset, ",")

		sql = sql + sqlset + " WHERE " + u[0]
		reg := regexp.MustCompile(`[']`)
		sql = reg.ReplaceAllString(sql, "`")
		tx, err := c.Db.Begin()
		if err != nil {
			Logger.Error(err)
			Logger.Debug(sql)
		}
		if _, err := tx.Exec(sql); err != nil {
			Logger.Error(err)
			Logger.Debug(sql)
		} else {
			if err = tx.Commit(); err != nil {
				Logger.Error(err)
				Logger.Debug(sql)
			}
			returndata = 1
		}
	} else {
		//Insert
		var sql string
		var inkey = " ("
		var inval = " ("
		sql = "INSERT INTO " + t

		for k, v := range kv {
			inkey = inkey + k + ","
			inval = inval + v.(string) + ","
		}

		inkey = strings.TrimRight(inkey, ",")
		sql = sql + inkey + ") VALUES"
		inval = strings.TrimRight(inval, ",")
		sql = sql + inval + ")"

		reg := regexp.MustCompile(`[']`)
		sql = reg.ReplaceAllString(sql, "`")

		tx, err := c.Db.Begin()
		if err != nil {
			Logger.Error(err)
			Logger.Debug(sql)
		}
		res, err := tx.Exec(sql)
		if err != nil {
			Logger.Error(err)
			Logger.Debug(sql)
			c.Close()
		} else {
			if err = tx.Commit(); err != nil {
				Logger.Error(err)
				Logger.Debug(sql)
			}
			reid, _ := res.LastInsertId()
			returndata = reid
		}
	}

	return returndata
}

//删除数据, return 还需要改为Save一样，设定一个变量，避免在错误是直接return.
func (c *Conn) Delete(t, w string) bool {
	//删除参数：1 表名、2 WHERE
	reg := regexp.MustCompile(`[']`)
	w = reg.ReplaceAllString(w, "`")

	checkdb := c.Query("SELECT * FROM " + t + " WHERE " + w)
	if len(checkdb) > 0 {

		tx, err := c.Db.Begin()
		if err != nil {
			Logger.Error(err)
			return false
		}
		if _, err := tx.Exec("DELETE FROM " + t + " WHERE " + w); err != nil {
			Logger.Error(err)
			return false
		} else {
			if err = tx.Commit(); err != nil {
				Logger.Error(err)
				return false
			}
			return true
		}
	} else {
		return false
	}
}
