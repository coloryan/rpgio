package mssql

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"reflect"
	"strconv"
	"strings"

	_ "github.com/denisenkom/go-mssqldb"
	"gitlab.com/magtil/rpgio/util/logger"
)

type Conn struct {
	db        *sql.DB
	DbHost    string
	DbPort    string
	DbName    string
	DbUser    string
	DbPass    string
	DbCharset string
	DbEncrypt string
	DbPrefix  string
}

var (
	Logger     = logger.Logger{}
	DateFormat = "2006/01/02/15:04:05"
)

//打开数据库
func (c *Conn) DbOpen() {
	dsn := "server=" + c.DbHost + ";user id=" + c.DbUser + ";password=" + c.DbPass + ";port=" + c.DbPort + ";database=" + c.DbName + ";encrypt=" + c.DbEncrypt + ";"
	db, err := sql.Open("mssql", dsn)
	if err != nil {
		Logger.Debug(err)
	} else {
		c.db = db
	}
}

//关闭数据库
func (c *Conn) DbClose() {
	Logger.Debug("close mssql")
	defer c.db.Close()
}

//数据执行
func (c *Conn) DbExec(sql string) []map[string]string {
	var rr = make([]map[string]string, 0)
	res, err := c.db.Exec(sql)
	if err != nil {
		Logger.Debug(err)
	} else {
		reid, _ := res.LastInsertId()
		Logger.Debug(reid)
		ra, _ := res.RowsAffected()
		Logger.Debug(ra)
	}
	return rr
}

//数据查询
func (c *Conn) DbQuery(sql string) []map[string]string {
	var rr = make([]map[string]string, 0)
	rows, err := c.db.Query(sql)
	defer rows.Close()
	if err != nil {
		Logger.Debug(err)
	} else {
		columns, _ := rows.Columns()
		scanArgs := make([]interface{}, len(columns))
		values := make([]interface{}, len(columns))
		for i := range values {
			scanArgs[i] = &values[i]
		}
		for rows.Next() {
			err = rows.Scan(scanArgs...)
			rp := make(map[string]string)
			for i, col := range values {
				if col != nil {
					rp[columns[i]] = fmt.Sprint(col)
				}
			}
			rr = append(rr, rp)
		}
	}
	return rr
}

//获得数据
func (c *Conn) Take(take map[string]string) []map[string]string {
	//查询参数：1 字段=field、2 表名=table、3 WHERE=where、4 LIMIT=limit、5 ORDER=order
	var rr = make([]map[string]string, 0)
	var sql string
	var field string
	if k, ok := take["field"]; ok {
		field = k
	} else {
		field = "*"
	}
	// table是必选项
	var where string
	if k, ok := take["where"]; ok {
		where = " WHERE " + k
	}
	var limit string
	if k, ok := take["limit"]; ok {
		limit = "TOP " + k + " "
	}
	var order string
	if k, ok := take["order"]; ok {
		order = " ORDER BY " + k
	}
	sql = "SELECT " + limit + field + " FROM [" + c.DbPrefix + take["table"] + "]" + where + order
	rows, err := c.db.Query(sql)

	defer rows.Close()
	if err != nil {
		Logger.Debug(err)
	} else {
		columns, _ := rows.Columns()
		scanArgs := make([]interface{}, len(columns))
		values := make([]interface{}, len(columns))
		for i := range values {
			scanArgs[i] = &values[i]
		}
		for rows.Next() {
			err = rows.Scan(scanArgs...)
			rp := make(map[string]string)
			for i, col := range values {
				if col != nil {
					rp[columns[i]] = fmt.Sprint(col)
				}
			}
			rr = append(rr, rp)
		}
	}
	return rr
}

//保存数据
func (c *Conn) Save(keyvalue interface{}, save ...string) int64 {
	//保存参数：1 keyvalue=值map 、2 save[0]=表、3 save[1]=WHERE
	//如果有WHERE就UPDATE，否则INSERT INTO
	var rint int64 = 0
	val := reflect.ValueOf(keyvalue)
	switch len(save) {
	case 1:
		var rr = make([]map[string]string, 0)
		var sql string
		var inkey = " ("
		var inval = " ("
		sql = "INSERT INTO " + c.DbPrefix + save[0]
		for i := 0; i < val.Len(); i++ {
			t := val.Index(i)
			tkey := t.Index(0).String()
			inkey = inkey + tkey + ","
		}
		inkey = strings.TrimRight(inkey, ",")
		sql = sql + inkey + ") VALUES"
		for i := 0; i < val.Len(); i++ {
			t := val.Index(i)
			tval := t.Index(1).String()
			inval = inval + tval + ","
		}
		inval = strings.TrimRight(inval, ",")
		sql = sql + inval + "); SELECT SCOPE_IDENTITY() AS [SCOPE_IDENTITY];"
		//res, err := c.db.Query(sql)

		rows, err := c.db.Query(sql)

		defer rows.Close()
		if err != nil {
			Logger.Debug(err)
			rint = 0
		} else {
			columns, _ := rows.Columns()

			scanArgs := make([]interface{}, len(columns))
			values := make([]interface{}, len(columns))
			for i := range values {
				scanArgs[i] = &values[i]
			}
			for rows.Next() {
				err = rows.Scan(scanArgs...)
				rp := make(map[string]string)
				for i, col := range values {
					if col != nil {
						rp[columns[i]] = string(col.([]uint8))
					}
				}
				rr = append(rr, rp)
			}
			if len(rr) > 0 {
				rint, _ = strconv.ParseInt(rr[0]["SCOPE_IDENTITY"], 10, 64)
			} else {
				rint = 0
			}
		}

	case 2:
		var rr = make([]map[string]string, 0)
		var sql string
		var sqlset = " SET "
		sql = "UPDATE " + c.DbPrefix + save[0]
		for i := 0; i < val.Len(); i++ {
			t := val.Index(i)
			tkey := t.Index(0).String()
			tval := t.Index(1).String()
			sqlset = sqlset + tkey + "=" + tval + ","
		}
		sqlset = strings.TrimRight(sqlset, ",")
		sql = sql + sqlset + " WHERE " + save[1] + "; SELECT SCOPE_IDENTITY() AS [REID];"
		uprows, err := c.db.Query(sql)

		defer uprows.Close()
		if err != nil {
			Logger.Debug(err)
			rint = 0
		} else {
			upcolumns, _ := uprows.Columns()

			scanArgs := make([]interface{}, len(upcolumns))
			upvalues := make([]interface{}, len(upcolumns))
			for i := range upvalues {
				scanArgs[i] = &upvalues[i]
			}
			for uprows.Next() {
				err = uprows.Scan(scanArgs...)
				rp := make(map[string]string)
				for i, col := range upvalues {
					if col != nil {
						rp[upcolumns[i]] = string(col.([]uint8))
					}
				}
				rr = append(rr, rp)
			}
			if len(rr) > 0 {
				rint, _ = strconv.ParseInt(rr[0]["REID"], 10, 64)
			} else {
				rint = 1
			}
			//暂时不理了，不知为什么UPDATE拿不到返回ID，查询的uprows里应该有了，就是循环不了出来
			rint = 1
		}
	default:
		rint = 0
	}
	return rint
}

//保存数据
func (c *Conn) OldSave(keyvalue interface{}, save ...string) int64 {
	//保存参数：1 keyvalue=值map 、2 save[0]=表、3 save[1]=WHERE
	//如果有WHERE就UPDATE，否则INSERT INTO
	val := reflect.ValueOf(keyvalue)
	switch len(save) {
	case 1:
		var sql string
		var inkey = " ("
		var inval = " ("
		sql = "INSERT INTO " + c.DbPrefix + save[0]
		for i := 0; i < val.Len(); i++ {
			t := val.Index(i)
			tkey := t.Index(0).String()
			inkey = inkey + tkey + ","
		}
		inkey = strings.TrimRight(inkey, ",")
		sql = sql + inkey + ") VALUES"
		for i := 0; i < val.Len(); i++ {
			t := val.Index(i)
			tval := t.Index(1).String()
			inval = inval + tval + ","
		}
		inval = strings.TrimRight(inval, ",")
		sql = sql + inval + ") SELECT @@IDENTITY"
		res, err := c.db.Query(sql)
		Logger.Debug(res)
		if err != nil {
			Logger.Debug(err)
			return 0
		} else {
			//reid, _ := res.LastInsertId()
			//Logger.Debug(reid)
			return 1
		}
	case 2:
		var sql string
		var sqlset = " SET "
		sql = "UPDATE " + c.DbPrefix + save[0]
		for i := 0; i < val.Len(); i++ {
			t := val.Index(i)
			tkey := t.Index(0).String()
			tval := t.Index(1).String()
			sqlset = sqlset + tkey + "=" + tval + ","
		}
		sqlset = strings.TrimRight(sqlset, ",")
		sql = sql + sqlset + " WHERE " + save[1]
		_, err := c.db.Query(sql)
		if err != nil {
			Logger.Debug(err)
			return 0
		} else {
			return 1
		}
	default:
		return 0
	}

}

//删除数据
func (c *Conn) Delete(table, where string) bool {
	//删除参数：1 表名、2 WHERE
	checkdb := c.DbQuery("SELECT '*' FROM " + c.DbPrefix + table + " WHERE " + where)
	if len(checkdb) > 0 {
		_, err := c.db.Query("DELETE FROM " + c.DbPrefix + table + " WHERE " + where)
		if err != nil {
			Logger.Debug(err)
			return false
		} else {
			return true
		}
	} else {
		return false
	}
}

func ReadConfig(filename string) (map[string]string, error) {
	gets := map[string]string{}
	bytes, err := ioutil.ReadFile(filename)
	if err != nil {
		Logger.Debug(err)
	}
	if err := json.Unmarshal(bytes, &gets); err != nil {
		Logger.Debug(err.Error())
		return nil, err
	}

	return gets, nil
}
