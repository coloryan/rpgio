package user

import (
	"crypto/md5"
	"encoding/hex"
	"strconv"
	"time"

	"gitlab.com/magtil/rpgio/util/db"
	"gitlab.com/magtil/rpgio/util/logger"
)

type User struct {
	PassObfus  string
	DateFormat string
	Db         db.Db
}

//初始化User结构体应该还有多个参数：base库和NoSQL库，NoSQL存储会话什么的。。。
var (
	Logger     = logger.Logger{}
	PassObfus  = "bullcalf"
	DateFormat = "2006/01/02/15:04:05"
	Db         db.Db
)

func (u User) Test() {

	Logger.Debug(u.Db.Take("base", "settings", map[string]string{}))

}

func (u User) CreatePassword(pass string) (string, string) {
	newLogTime := strconv.FormatInt(time.Now().Unix(), 10)
	newPass := md5.New()
	newPass.Write([]byte(pass + u.PassObfus + newLogTime)) //登录验证的password
	toNewPass := hex.EncodeToString(newPass.Sum(nil))      //最终MD5值
	return newLogTime, toNewPass
}

func (u User) CheckPassword(logtime int64, textpass, enpass string) bool {
	dbLogTime := strconv.FormatInt(logtime, 10)
	mdpass := md5.New()
	mdpass.Write([]byte(textpass + u.PassObfus + dbLogTime)) //登录验证的password
	valid := hex.EncodeToString(mdpass.Sum(nil))             //最终MD5值
	if valid == enpass {
		return true
	} else {
		return false
	}
}

func Init(cfg User) User {
	var (
		setPassObfus  *string = &PassObfus
		setDateFormat *string = &DateFormat
		setDb         *db.Db  = &Db
	)
	if len(cfg.PassObfus) > 0 {
		*setPassObfus = cfg.PassObfus
	}
	if len(cfg.DateFormat) > 0 {
		*setDateFormat = cfg.DateFormat
	}
	*setDb = cfg.Db
	return cfg
}
