package str

import (
	"strconv"
	"time"
)

type Str struct {
}

func (u Str) StringTimeToUnix(tstring, tformat string) string {
	totimetype, _ := time.ParseInLocation(tformat, tstring, time.Local)
	tounix := strconv.FormatInt(totimetype.Unix(), 10)
	return tounix
}

func (u Str) StringTimeToUnixNotLocal(tstring, tformat string) string {
	totimetype, _ := time.Parse(tformat, tstring)
	tounix := strconv.FormatInt(totimetype.Unix(), 10)
	return tounix
}
